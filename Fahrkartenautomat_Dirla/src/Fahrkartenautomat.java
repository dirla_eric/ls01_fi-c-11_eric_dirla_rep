import java.util.Scanner;
public class Fahrkartenautomat {
	static Scanner tastatur = new Scanner(System.in);
	static double eingezahlterGesamtbetrag;
	public static double fahrkartenbestellungErfassen(double kostenProTicket, short anzahlTickets) {
		return kostenProTicket * anzahlTickets;
	}
	public static void fahrkartenbestellungBezahlen(double zuZahlen) {
		eingezahlterGesamtbetrag = 0.0;
		while(eingezahlterGesamtbetrag < zuZahlen)
		{
			double eingeworfeneM�nze;
			double e = zuZahlen - eingezahlterGesamtbetrag;
			System.out.printf("Noch zu zahlen: " + "%.2f", e);System.out.println(" Euro");
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
	}
	public static void fahrkartenbestellungAusgeben(short anzahlTickets) {
	       if(anzahlTickets > 1.9) {System.out.println("\nFahrscheine werden ausgegeben");}
	       else {System.out.println("\nFahrschein wird ausgegeben");}
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
	       double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(r�ckgabebetrag > 0.0)
	       {
	    	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " Euro");
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");
	    	   
	    	   while(r�ckgabebetrag >=30) {System.out.println("30 Euro Schein"); r�ckgabebetrag -= 30.0;}

	           while(r�ckgabebetrag >= 2.0) // 2 Euro-M�nzen
	           {
	        	  System.out.println("2 Euro");
		          r�ckgabebetrag -= 2.0;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 Euro-M�nzen
	           {
	        	  System.out.println("1 Euro");
		          r�ckgabebetrag -= 1.0;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.5;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.2;
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.1;
	           }
	           while(r�ckgabebetrag >= 0.048)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	           }

	       }

	       System.out.println("\nVergessen Sie den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen keine gute Fahrt.");
	    }
	
	public static void main(String[] args) {
		
		double kostenProTicket;
		short anzahlTickets;
	    System.out.print("Zu zahlender Betrag (Euro): ");
	    kostenProTicket = tastatur.nextDouble();
	    System.out.print("Anzahl der Fahrkarten: ");
	    anzahlTickets = tastatur.nextShort();
	    double zuZahlen = fahrkartenbestellungErfassen(kostenProTicket, anzahlTickets);
	    fahrkartenbestellungBezahlen(zuZahlen);
	    fahrkartenbestellungAusgeben(anzahlTickets);
	    rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlen);
	    
	}
}