import java.util.Scanner;

public class Wiederstandsberechnung
{
    public static void main(String[] args)
    {
       String groe�e;
       double u;
       double i;
       double r;
       Scanner tastatur = new Scanner(System.in);
       System.out.println("U, R oder I berechnen?");
       groe�e = tastatur.nextLine();
       if(groe�e == "U") {
    	   System.out.println("Wie gro� ist R in Ohm?");
    	   r = tastatur.nextDouble();
    	   System.out.println("Wie gro� ist I in Ampere?");
    	   i = tastatur.nextDouble();
    	   u = i * r;
    	   System.out.println("Das Ergebnis ist " + u + " Volt.");
       }
       else if(groe�e == "R") {
    	   System.out.println("Wie gro� ist U in Volt?");
    	   u = tastatur.nextDouble();
    	   System.out.println("Wie gro� ist I in Ampere?");
    	   i = tastatur.nextDouble();
    	   r = u / i;
    	   System.out.println("Das Ergebnis ist " + r + " Ohm.");
       }
       else if(groe�e == "I") {
    	   System.out.println("Wie gro� ist U in Volt?");
    	   u = tastatur.nextDouble();
    	   System.out.println("Wie gro� ist R in Ohm?");
    	   r = tastatur.nextDouble();
    	   i =  u / r;
    	   System.out.println("Das Ergebnis ist " + i + " Ampere.");
       }
       else {System.out.println("Fehler.");}
      tastatur.close();
    }
}